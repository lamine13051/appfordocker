import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Label;
import java.awt.Panel;

import javax.swing.JFrame;

class Fenetre extends JFrame {
	public Fenetre() {
		this.setTitle("App for docker");
		this.setSize(500, 200);
		Panel p = new Panel();
		Label lab = new Label("		Cette application tourne sous docker");
		lab.setBackground(Color.green);
		p.setBackground(Color.cyan);
		p.setLayout(new BorderLayout());
		p.add(lab, BorderLayout.CENTER);
		this.add(p);
		this.setVisible(true);
	}
}

public class Main {

	public static void main(String[] args) {
		Fenetre fenetre = new Fenetre();

	}

}
